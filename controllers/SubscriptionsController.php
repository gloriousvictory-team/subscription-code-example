<?php

class SubscriptionsController extends \BaseController
{

  public function subscribe()
  {

    $user = Auth::user();
    $user_group = $user->usergroup;

    if($user_group->hasCardOnStripe()) {
      // If the user already has card details, redirect them to the company page with alert
      return Redirect::route('group.view', array($user_group->id))->with('message', 'You\'re already subscribed');
    }

    // Get users country from GeoIP DB
    $geo_service = new GeolocationService;
    $country = $geo_service->getCountryByIP();

    $subscriptionCountry = SubscriptionCountriesFactory::getSubscriptionCountryByCountry($country);

    // Need this to make sure we get upgraded to the country specific plan
    // save ISO code as country_code against company
    $user_group->country_code = $subscriptionCountry->getIsoCode();
    $user_group->save();

    $monthlyplan = $subscriptionCountry->getSubscriptionPlan();

    $discount = false;

    if($user_group->getStripeSubscription()) {

      if($discount = $user_group->getStripeSubscription()->discount) {
        $subscription_cost = $subscription_cost * ($discount->coupon->percent_off/100);
      }

    }

    // List countries in form
    $subscription = new Subscription;
    $countries = $subscription->getCountries();

    return View::make('subscriptions.select_subscription', compact('countries', 'monthlyplan', 'user_group', 'discount', 'subscriptionCountry'));


    /*
    // show modal and let Quaderno deal with it
    if(Auth::user()->isReadOnly()) {

      return View::make('subscriptions.subscribe');

    }

    return Redirect::to('/'); */

  }

}
