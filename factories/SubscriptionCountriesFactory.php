<?php

class SubscriptionCountriesFactory
{

  private $_subscriptionCountries = [];

  public function addSubscriptionCountryWithKey(SubscriptionCountry $subscriptionCountry, $key)
  {

    $this->_subscriptionCountries[$key] = $subscriptionCountry;

  }

  public function getSubscriptionCountryByKey($key)
  {

    if(isset($this->_subscriptionCountries[$key])){

      return $this->_subscriptionCountries[$key];

    }


  }

  public static function getSubscriptionCountryByCountry($country = false)
  {

    $subscriptionFactory = new SubscriptionCountriesFactory;
    $subscriptionFactory->addSubscriptionCountryWithKey(new SubscriptionCountryUS, 'US');

    if($country) {

      $subscriptionCountry = $subscriptionFactory->getSubscriptionCountryByKey($country->country->isoCode);

      if($subscriptionCountry) {

        $subscriptionCountry->setCountryFromGeoLocationModel($country);

        return $subscriptionCountry;

      }

    }

    return new SubscriptionCountryDefault;
  }

}
