# Readme

## Context

This is a snippet of live production code used to determine which subscription plan a customer should be assigned to based on their country ISO code.

Each country has it's own subscription plan, currency and pricing, and we needed the flexibility to add new subscriptions per country as our product grew.

Originally extracted from a Laravel 4.2 project.

**This code will not run as is and is intended to show structure only**
