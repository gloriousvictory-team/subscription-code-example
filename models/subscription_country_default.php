<?php

class SubscriptionCountryDefault extends SubscriptionCountry
{

  public function getIsoCode()
  {
    return 'GB';
  }

  public function getCurrencySymbol()
  {
    return "£";
  }

  public function getSubscriptionPlan()
  {
    return StripePlanService::getStarterPlanByCountry();
  }

}
