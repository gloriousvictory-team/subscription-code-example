<?php

abstract class SubscriptionCountry
{

  protected $_country;
  protected $_subscription_plan;


  public function setCountryFromGeoLocationModel($country)
  {
    $this->_country = $country;
  }

  public function getCurrencyName()
  {
    return $this->getSubscriptionPlan()->currency;
  }

  public function getIsoCode()
  {
    return $this->_country->country->isoCode;
  }

  public function getCurrencySymbol()
  {
    return false;
  }

  public function getPrice()
  {
    return $this->getSubscriptionPlan()->amount/100;
  }

  public function getSubscriptionPlan()
  {
    return StripePlanService::getStarterPlanByCountry($this->_country);
  }

}
