<?php

class StripePlanService
{

  public static function getStarterPlanByCountry($country = false)
  {

    $plan = 'starter';
    $isoCode = false;

    if($country) {

      $isoCode = $country->country->isoCode;

    }

    return StripePlanService::getPlanByIsoCode($plan, $isoCode);

  }

  public static function getPlanByIsoCode($plan = 'starter', $iso_code = false)
  {

    if($iso_code) {
      $monthlyplan_name = $plan.'_'.strtoupper($iso_code);

      if($stripe_plan = Subscription::find($monthlyplan_name)) {

        $plan = $monthlyplan_name;

      }

    }

    return Stripe_Plan::retrieve($plan);

  }

}
